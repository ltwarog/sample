<div id="visualization" class="interactive-img-preview"><svg id="svg"></svg></div>
<script src="/js/raphael.min.js" type="text/javascript" ></script>
<script>
var imageWidth = 1920;
var imageHeight = 909;
var imageRatio = imageHeight / imageWidth;
var actualWidth, actualHeight = null;
var pointRatio = '';
var paper = null;
var coords =[["424,429,354,698,677,839,745,425,1086,473,530,328", 12,"link"]] ;
var rimage;
var tmp;
$(document).ready(function () {
	actualWidth = $('#visualization').width();
	if (actualWidth > imageWidth) {
		actualWidth = imageWidth;
	}
	actualHeight = actualWidth * imageRatio;
	pointRatio = actualWidth / imageWidth;
	$('#svg').css({
		'width': actualWidth,
		'height': actualHeight
	});

	paper = Raphael("svg", actualWidth, actualHeight);
	paper.setViewBox(0, 0, actualWidth, actualHeight, true);

	rimage = paper.image("3faa8804421635257ab25a4cf9cb10f9.jpg", 0, 0, actualWidth, actualHeight);

	loadPolygons();
	$(window).resize(function () {
		redrawVisualization(paper);
	}).trigger("resize");
});

function convertPolyToPath(points_string) {
	var tmp = points_string.split(/,/);
	var points_tmp = '';
	$.each(tmp, function (index, value) {
		if (index != 0) {
			if ((index % 2) == 0) {
				points_tmp += ' ';
			} else {
				points_tmp += ',';
			}
		}
		points_tmp += value;
	});

	var points = points_tmp.split(/\s+/);
	var resized = [];
	$.each(points, function (index, value) {
		var point = value.split(/,/);
		point[0] = parseFloat(point[0]) * pointRatio;
		point[1] = parseFloat(point[1]) * pointRatio;
		resized.push(point);
	});
	var pathdata = 'M ' + resized.join(' ');
	pathdata += 'z';
	pathdata = pathdata.trim();
	return pathdata;
}

function loadPolygons() {
	$.each(coords, function (index, value) {
		var mark_tmp = paper.path(convertPolyToPath(value[0]));
		tmp = mark_tmp;
		mark_tmp.attr({
			"stroke-width": 0,
			"fill": "#e4952e",
			"fill-opacity": 0
		});
		mark_tmp.node.id = value[1] + '-svg';

		if (value[3]) {
			mark_tmp.click(function () {
				window.location.href = value[3];
			});
		}

		mark_tmp.mouseover(function () {
			$(rimage[0]).addClass("grayscale");
			mark_tmp.animate({"fill-opacity": 0.7}, 100, function () {
				mark_tmp.attr("fill-opacity", 0.7);
			});
		});

		mark_tmp.mouseout(function () {
			$(rimage[0]).removeClass("grayscale");
			mark_tmp.animate({"fill-opacity": 0}, 100, function () {
				mark_tmp.attr("fill-opacity", 0);
			});
			mark_tmp.attr("fill-opacity", 0);
		});
	});
}

function redrawVisualization(paper) {
	actualWidth = $('#visualization').width();
	if (actualWidth > imageWidth) {
		actualWidth = imageWidth;
	}
	actualHeight = actualWidth * imageRatio;

	paper.setSize(actualWidth, actualHeight);
	$('#svg').css({
		'width': actualWidth,
		'height': actualHeight
	});
}
</script>