<?php

namespace AppBundle\Command;

use AppBundle\Entity\Author;
use AppBundle\Entity\ErrorAvatars;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;

use PHPThumb\GD;

class AvatarsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('DownloadAvatars')
            ->setDescription('download avatars to disk');
    }
    
    const AVATAR_WIDTH = 50;
    const AVATAR_HEIGHT = 50;
    
    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        try{
            $list = $em->getRepository("AppBundle:Author")->findAvatarToDownload(50);
            
            foreach($list as $user){
                if(!file_exists($user->getPicture())){
                    
					$picture = $this->apiUserPicture($user);
                }
                
                $thumb = new GD($user->getPicture());
                
                $thumb->resize(self::AVATAR_WIDTH, self::AVATAR_HEIGHT);
                
                switch (strtolower($thumb->getFormat())){
                    case "png":
                        $ext = "png";
                        break;
                    case "gif":
                        $ext = "gif";
                        break;
                    default:
                        $ext = "jpg";
                        break;
                }
                
                $file='avatar_'.$user->getId().'.'.$ext;
                
                $thumb->save($this->getAvatarPath($user).$file, $ext);
                
                $user->setDownloadPicture(true);
                $user->setOrginalPicture($user->getPicture());
                $user->setPicture($this->getAvatarPath($user,true).$file);
                $em->flush();
            }
            
        } catch (\Exception $e) {
            $error = new ErrorAvatars();

            $content = $e->getMessage().'
            '.$e->getTraceAsString();

            $error->setContent($content);
            $error->setType($user->getId());
            $error->setCreatedDate(new \DateTime());
            $em->persist($error);
            
            //ustawienie defaulta
            $file='avatar_'.$user->getId().'.jpg';
            copy(__DIR__.'/../../../web/avatars/default.jpg', $this->getAvatarPath($user).$file);
            
            $user->setDownloadPicture(true);
            $user->setOrginalPicture($user->getPicture());
            $em->flush();
            
            $em->flush();
            exit;
        }
    }
    
    private function getAvatarPath($user, $absolute=false){
        $path = __DIR__.'/../../../web/avatars/channels';

        $folder = ceil($user->getId()/1000);

        if (!file_exists($path.'/'.$folder)) {
            if (!mkdir($path.'/'.$folder, 0777, true)) {
                throw new Exception("Błąd utworzenia folderu: ".$folder);
            }
        }
        
        if($absolute){
            $path = '/avatars/channels';
        }
        
        return $path.'/'.$folder.'/';
    }
    
    private function apiUserPicture($user)
    {
        /*
		return url to picture
		*/
    }
}
