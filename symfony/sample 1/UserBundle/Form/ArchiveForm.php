<?php
namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Post;
use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ArchiveForm extends AbstractType
{
   
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('channels',EntityType::class,[
            'required'=>false,
            'expanded'=>false,
            'multiple'=>true,
            'class' => 'AppBundle:Channel',
            'choice_label' => 'name',
        ]);
        
        $builder->add('status',ChoiceType::class,[
            'required'=>false,
            'expanded'=>false,
            'multiple'=>true,
            'choices' => [
                'open' => 'open',
                'closed' => 'closed',
            ],
        ]);
        
        $builder->add('other',ChoiceType::class,[
            'required'=>false,
            'expanded'=>false,
            'multiple'=>true,
            'choices' => [
                'note' => 'note',
                'attachment' => 'attachment',
                'none' => 'none',
            ],
        ]);
            
        $builder->add('date_from',DateType::class,[
            'label' => 'archive.form.from',
            'required'=>false,
            'widget' => 'single_text',
            'html5' => false,
            'format' => 'yyyy-MM-dd',
            'translation_domain'   => 'UserBundle',
        ]);
        $builder->add('date_to',DateType::class,[
            'label' => 'archive.form.to',
            'required'=>false,
            'widget' => 'single_text',
            'html5' => false,
            'format' => 'yyyy-MM-dd',
            'translation_domain'   => 'UserBundle',
        ]);
        
        $builder->add('search',TextType::class,[
            'required'=>false,
        ]);
    }

     public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'archive_form';
    }
}