<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Config;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Post;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

use UserBundle\Form\ArchiveForm;

/**
 * @Config\Route("/archive", name="archive_")
 */
class ArchiveController extends Controller
{
    /**
     * @Config\Route("/{page}", name="index", requirements={"page": "\d+"}, defaults={"page"=1})
     * @Config\Method({"GET"})
     */
    public function indexAction(Request $request, $page)
    {
        /** @var EventManager **/
        $em = $this->getDoctrine()->getManager();
        
        $form = $this->createForm(ArchiveForm::class, null, array(
            'method' => 'GET',
            'action' => $this->generateUrl("archive_index",)
        ));
        $form->handleRequest($request);
        

        $data = null;
        
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }
        
        $query  = $em->getRepository(Post::class)->getListArchive($this->getUser(), $data);
        
	    $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', $page),
            10
        );

        $pagination->setUsedRoute('archive_index');


        $content = $this->renderView(
            'UserBundle:Archive:list.html.twig',
            array(
                'list' => $pagination,
	            'owners' => $owners
            )
        );
        

        return $this->render(
            'UserBundle:Archive:index.html.twig',
            array(
                'page' => $page,
                'content' => $content,
                'form' => $form->createView(),
            )
        );
    }


}