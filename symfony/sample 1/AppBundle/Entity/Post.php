<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Gedmo\Tree(type="closure")
 * @Gedmo\TreeClosure(class="AppBundle\Entity\PostClosure")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PostRepository")
 */
class Post
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * This parameter is optional for the closure strategy
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Gedmo\TreeLevel
     */
    private $level;

    /**
     * @Gedmo\TreeParent
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="RESTRICT")
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="children")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="parent")
     */
    private $children;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Channel", inversedBy="posts")
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id", nullable=false)
     */
    protected $channel;

    /**
     * @ORM\ManyToOne(targetEntity="Author", inversedBy="posts", cascade={"persist"})
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    protected $author;

    /**
     * @ORM\Column(type="text", columnDefinition="nvarchar(max)", nullable=true)
     */
    protected $content;
   

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $createdTime;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $deleted = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $rating = null;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="posts", cascade={"all"})
     * @ORM\JoinTable(name="post_tags")
     */
    private $tags;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $postClose = false;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $additionalData;
    
    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }


    public function getId()
    {
        return $this->id;
    }

    public function setParent(Post $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    function getChildren()
    {
        return $this->children;
    }

    function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }


    public function addClosure(PostClosure $closure)
    {
        $this->closures[] = $closure;
    }

    public function setLevel($level)
    {
        $this->level = $level;
    }

    public function getLevel()
    {
        return $this->level;
    }

    function getChannel()
    {
        return $this->channel;
    }

    function setChannel($channel)
    {
        $this->channel = $channel;
        return $this;
    }

    function getContent()
    {
        return $this->content;
    }

    function getDeleted()
    {
        return $this->deleted;
    }

    function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    function getRating()
    {
        return $this->rating;
    }

    function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;

        return $this;
    }

    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    public function setAuthor(Author $author = null)
    {
        $this->author = $author;
        return $this;
    }

    public function getAuthor()
    {
        return $this->author;
    }

	

    function getType()
    {
        return $this->type;
    }

    function setType($type)
    {
        $this->type = $type;
        return $this;
    }


    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;
        return $this;
    }

    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
        return $this;
    }

    public function removeAllTags()
    {
        $this->tags->clear();
        return $this;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function setTags(ArrayCollection $tags)
    {
        $this->tags = $tags;
        return $this;
    }


    function getPostClose()
    {
        return $this->postClose;
    }

    function setPostClose($postClose)
    {
        $this->postClose = $postClose;
        return $this;
    }


    function getAdditionalData() {
        return $this->additionalData;
    }

    function setAdditionalData($additionalData) {
        $this->additionalData = $additionalData;
        return $this;
    }
}
