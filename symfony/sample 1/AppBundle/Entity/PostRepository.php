<?php

namespace AppBundle\Entity;

use Gedmo\Tree\Entity\Repository\ClosureTreeRepository;

use AppBundle\Entity\User;
use AppBundle\Entity\Channel;
use AppBundle\Entity\Post;
use AppBundle\Entity\PostClosure;
use AppBundle\Entity\Author;
use Doctrine\Common\Collections\ArrayCollection;

class PostRepository extends ClosureTreeRepository
{

    public function getListArchive(User $user, $filters = []){
	    $em = $this->getEntityManager();

	    $query = $em->getRepository(Post::class)->createQueryBuilder('post')->select('post')
	                ->innerJoin("post.channel", 'channel')->addSelect("channel")
	                ->innerJoin("post.author", 'author')->addSelect("author")
	                ->leftJoin("post.owner", 'owner')
	                ->leftJoin("post.parent", 'parent')->addSelect("parent")
	               
	                ->where(("post.deleted=false")
	                
		            ->orderBy("post.postClose", "DESC")
	                ->addOrderBy("post.createdTime", "DESC");
					
        $query->andWhere($query->expr()->in("channel", ':channelsList'));
	    
        if(isset($filters['channels']) && is_a($filters['channels'], ArrayCollection::class) && !$filters['channels']->isEmpty()){
            $query->setParameter("channelsList", $filters['channels']);
            
        }else{
            $query->setParameter("channelsList", $user->getChannels()); 
        }
        
        
	    if(isset($filters['status']) && !empty($filters['status'])){
            if(in_array('open', $filters['status']) &&  in_array('closed', $filters['status'])){
                $query->andWhere("(post.postClose=true or post.postClose=false)");
            }
            elseif(in_array('open', $filters['status'])){
                $query->andWhere("post.postClose=false");
            }
            elseif(in_array('closed', $filters['status'])){
                $query->andWhere("post.postClose=true");
            }
        }
       
        
        if(isset($filters['date_from']) && is_a($filters['date_from'], \DateTime::class)){
            $query->andWhere("post.createdTime>=:dateFrom")->setParameter("dateFrom", new \DateTime($filters['date_from']->format("Y-m-d").' 00:00:00'));
        }

        if(isset($filters['date_to']) && is_a($filters['date_to'], \DateTime::class)){
            $query->andWhere("post.createdTime<=:dateTo")->setParameter("dateTo", new \DateTime($filters['date_to']->format("Y-m-d").' 23:59:59'));
        }
        
        if(isset($filters['search']) && trim($filters['search'])!=''){
            $query->andWhere($query->expr()->like("post.content", ':contentText'))->setParameter("contentText", "%".trim($filters['search'])."%");
        }
        
	    return $query->getQuery();
    }

   
    public function getPostParent(Post $post, $returnResult = true){

        $em = $this->getEntityManager();

        $query = $em->getRepository(PostClosure::class)->createQueryBuilder('postc')
                ->leftJoin("postc.ancestor", 'post')->addSelect("post")
                ->where("postc.depth=:depth")->setParameter("depth", ($post->getLevel()-1))
                ->andWhere("postc.descendant=:descendant")->setParameter("descendant", $post);

        if($returnResult){
            return $query->getQuery()->getOneOrNullResult();
        }else{
            return $query;
        }
    }
}
