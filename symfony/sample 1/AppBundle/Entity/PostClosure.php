<?php

namespace AppBundle\Entity;

use Gedmo\Tree\Entity\MappedSuperclass\AbstractClosure;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class PostClosure extends AbstractClosure
{
    /**
     * @ORM\ManyToOne(targetEntity="Post")
     * @ORM\JoinColumn(name="ancestor", referencedColumnName="id", unique=false, nullable=false, onDelete="RESTRICT", columnDefinition=null)
     */
    protected $ancestor;

    /**
     * @ORM\ManyToOne(targetEntity="Post")
     * @ORM\JoinColumn(name="descendant", referencedColumnName="id", unique=false, nullable=false, onDelete="RESTRICT", columnDefinition=null)
     */
    protected $descendant;

    /**
     * @ORM\Column(type="integer")
     */
    protected $depth;
}
