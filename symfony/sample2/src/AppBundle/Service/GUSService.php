<?php
namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;

use GusApi\GusApi;
use GusApi\RegonConstantsInterface;
use GusApi\Exception\InvalidUserKeyException;
use GusApi\ReportTypes;
use GusApi\ReportTypeMapper;

use AppBundle\Entity\Company;

class GUSService {
   
    protected $em;
    protected $gus;
    
    public function __construct(EntityManager $em, $key, $type)
    {
        $this->em = $em;
        
        if($type == 'test'){
            $this->gus = new GusApi(
                $key,
                new \GusApi\Adapter\Soap\SoapAdapter(
                    RegonConstantsInterface::BASE_WSDL_URL_TEST,
                    RegonConstantsInterface::BASE_WSDL_ADDRESS_TEST
                )
            );
        }else{
            $this->gus = new GusApi(
                $key,
                new \GusApi\Adapter\Soap\SoapAdapter(
                    RegonConstantsInterface::BASE_WSDL_URL,
                    RegonConstantsInterface::BASE_WSDL_ADDRESS
                )
            );
        }
    }
    
    public function serchForEntity(Company $company){
        if(!empty($company->getNip())){
            $data = $this->nipSearch($company->getNip());
            
            if($this->notEmptyArray($data)){
                if(!empty($data['name'])){
                    $company->setName($data['name']);
                }
                if(!empty($data['regon'])){
                    $company->setRegon($data['regon']);
                }
                if(!empty($data['city'])){
                    $company->setCity($data['city']);
                }
                if(!empty($data['postCode'])){
                    $company->setPostCode($data['postCode']);
                }
                if(!empty($data['street'])){
                    $company->setAddress($data['street']);
                }
                if(!empty($data['phone'])){
                    $company->setPhone($data['phone']);
                }
                if(!empty($data['mail'])){
                    $company->setMail($data['mail']);
                }
                if(!empty($data['www'])){
                    $company->setWebSite($data['www']);
                }
            }
        }
    }
    
    private function notEmptyArray($data){
        if($data == null || !$data || !is_array($data)){
            return false;
        }
        
        foreach($data as $key=>$val){
            if(!empty($val)){
                return true;
                break;
            }
        }
        return false;
    }


    private function nipSearch(string $nipToCheck){
        try {
            $return = [];
            
            $mapper = new ReportTypeMapper();
            $sessionId = $this->gus->login();

            $gusReports = $this->gus->getByNip($sessionId, $nipToCheck);

            foreach ($gusReports as $gusReport) {
                $reportType = $mapper->getReportType($gusReport);
                $fullReport = $this->gus->getFullReport($sessionId, $gusReport, $reportType)->dane;

                $return['name'] = (string) $gusReport->getName();
                $return['regon'] = (int) $gusReport->getRegon();
                $return['city'] = (string) $gusReport->getCity();
                $return['postCode'] = (string) $gusReport->getZipCode();
                $return['street'] = (string) $gusReport->getStreet();
                $return['phone'] = (string) $fullReport->fiz_numerTelefonu;
                $return['mail'] = (string) $fullReport->fiz_adresEmail;
                $return['www'] = (string) $fullReport->fiz_adresStronyinternetowej;

                return $return;
            }
        } catch (\Exception $e) {
            return null;
        }
    }
}